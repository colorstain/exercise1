# Exercise 1: Capitals game

## Setup

- Install PyYaml: `pip install pyyaml`

## Try it

Run `python capitals_game.py`

## Exercises:

- Keep track of number of correct guesses. Display how many correct guesses the user had.
- Give a hint, display the first letter of the answer when asking a question.
- Make the game case insensitive.
- Ask the user how many questions they want.
