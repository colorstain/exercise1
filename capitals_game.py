import random
import yaml


def load_data():
    with open('data.yaml') as f:
        return yaml.load(f)


def main():
    data = load_data()

    for i in [0, 1, 2]:
        state_capital = random.choice(data)

        guess = input('What is the capital of ' + state_capital['state'] + '? ')

        if guess == state_capital['capital']:
            print('Correct! You paid attention in Geography class.')
        else:
            print('Incorrect. The capital of ' + state_capital['state'] + ' is ' + state_capital['capital'] + '.')

    print('Done')


if __name__ == '__main__':
    main()
